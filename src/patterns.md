---
marp: true
theme: lection
paginate: true

---

<!-- _class: lead -->
<!-- _paginate: false -->

# Паттерны проектирования


#

#

#

#

#

#

#

##### Константин Володин

---

# HW Recap: Задание лр / на неделю / ДЗ

1. Прочитать доступные источники по теме занятия

2. Для каждого из принципов изучить код лаборторных работ на предмет наличия нарушения его.

3. Для каждого из принципов предложить улучшение кода лабораторных работ.

---

# Принципы vs Паттерны

__Design Principles:__

1. Design principles are core abstract principles that we are supposed to follow while designing software. 
2. Remember they aren't concrete - rather abstract. 
3. They can be applied in any language, on any platform regardless of the state as long as we are within the permissible conditions.

__Examples:__

1. Encapsulate what varies.
2. Program to interfaces, not to implementations.
3. Depend upon abstractions. Do not depend upon concrete classes.

---

# Принципы vs Паттерны

__Design Patterns:__

1. They are solutions to real-world problems that pop up time and again, so instead of reinventing the wheel, we follow the design patterns that are well-proven, tested by others, and safe to follow. 
2. Now, design patterns are specific; there are terms and conditions only in which a design pattern can be applied.

__Examples:__

1. Singleton Pattern (One class can only have one instance at a time)
2. Adapter Pattern (Match interface of different classes)

---

<!-- _paginate: false -->

# Паттерны проектирования

__Паттерн проектирования__ — это часто встречающееся решение определённой проблемы при проектировании архитектуры программ.

В __1994__ году  __Эрих Гамма, Ричард Хелм, Ральф Джонсон, Джон Влиссидес__ написали книгу __«Приемы объектно-ориентированного проектирования. Паттерны проектирования»__, в которую вошли __23 паттерна__, решающие различные проблемы объектно-ориентированного дизайна. 

Название книги было слишком длинным, чтобы кто-то смог всерьёз его запомнить. Поэтому вскоре все стали называть её __«book by the gang of four»__, то есть «книга от банды четырёх», а затем и вовсе __«GoF book»__.

---

# Паттерны

__Паттерны__ часто путают с __алгоритмами__, ведь оба понятия описывают типовые решения каких-то известных проблем. Но если __алгоритм__ — это __чёткий набор действий__, то __паттерн__ — это __высокоуровневое описание решения__, реализация которого может отличаться в двух разных программах.

<br>
<br>
<br>

## Пример: оператор FOR

---

<!-- _class: oneline-center -->

# Книги

![h:270px](img/refactoring.png) ![h:480px](img/patterns.jpeg) ![h:480px](img/patterns2.jpg)

---

# Группы паттернов

1. __Порождающие паттерны__ беспокоятся о гибком создании объектов без внесения в программу лишних зависимостей.

2. __Структурные паттерны__ показывают различные способы построения связей между объектами.

3. __Поведенческие паттерны__ заботятся об эффективной коммуникации между объектами.


---

# Порождающие паттерны (creational)

Порождающие паттерны паттерны отвечают за удобное и безопасное создание новых объектов или даже целых семейств объектов.

1. Фабричный метод (Factory method)
2. Абстрактная фабрика (Abstract factory)
3. Строитель (Builder)
4. Прототип (Prototype)
5. Одиночка (Singleton)

---

# Структурные паттерны (structure)

Структурные паттерны паттерны отвечают за построение удобных в поддержке иерархий классов.

1. Адаптер (Adapter)
2. Мост (Bridge)
3. Компоновщик (Composite)
4. Декоратор (Decorator)
5. Фасад (Facade)
6. Легковес (Flyweight)
7. Заместитель (Proxy)

---
# Поведенческие паттерны (behavior)

1. Цепочка обязанностей (Chain of Responsibility)
2. Команда (Command)
3. Итератор (Iterator)
4. Посредник (Mediator)
5. Снимок (Memento)
6. Наблюдатель (Observer)
7. Состояние (State)
8. Стратегия (Stratery)
9. Шаблонный метод (Template Method)
10. Посетитель (Visitor)

---

# Стратегия (Strategy pattern)

__Стратегия__ — это поведенческий паттерн проектирования,который определяет семейство схожих алгоритмов и помещает каждый из них в собственный класс, после чего алгоритмы можно взаимозаменять прямо во время исполнения программы.

---

```python
import string
import random
from typing import List


def generate_id(length: str = 8) -> str:
    return ''.join(random.choices(string.ascii_uppercase, k=length))


class SupportTicket:
    id: str
    customer: str
    issue: str
    
    def __init__(self, customer, issue):
        self.id = generate_id()
        self.customer = customer
        self.issue = issue
```

---

```python
class CustomerSupport:
    tickets : List[SupportTicket] = []

    def create_ticket(self, customer, issue):
        self.tickets.append(SupportTicket(customer, issue))

    def process_tickets(self, processing_strategy : str = 'fifo'):
        if len(self.tickets) == 0:
            print("There are no tickets to process. Well done!")
            return

        if self.processing_strategy == "fifo":
            for ticket in self.tickets:
                self.process_ticket(ticket)
        elif self.processing_strategy == "filo":
            for ticket in reversed(self.tickets):
                self.process_ticket(ticket)
        elif self.processing_strategy == "random":
            list_copy = self.tickets.copy()
            random.shuffle(list_copy)
            for ticket in list_copy:
                self.process_ticket(ticket)

    def process_ticket(self, ticket: SupportTicket):
        print("==================================")
        print(f"Processing ticket id: {ticket.id}")
        print(f"Customer: {ticket.customer}")
        print(f"Issue: {ticket.issue}")
        print("==================================")
```
---

```python
app = CustomerSupport()

app.create_ticket("John Smith", "My computer makes strange sounds!")
app.create_ticket("Linus Sebastian", "I can't upload any videos, please help.")
app.create_ticket("Arjan Egges", "VSCode doesn't automatically solve my bugs.")

app.process_tickets("filo")
```

---

<!-- _class: contacts -->

# Диаграмма классов примера

<br>
<br>

![w:1230px](diagrams/strategy-example-1.png)


---

<!-- _class: contacts -->

# Диаграмма классов примера применения паттерна

![w:1100px](diagrams/strategy-example-2.png)

---

```python
class TicketOrderingStrategy(ABC):
    @abstractmethod
    def create_ordering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        pass


class FIFOOrderingStrategy(TicketOrderingStrategy):
    def create_ordering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        return list.copy()


class FILOOrderingStrategy(TicketOrderingStrategy):
    def create_ordering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        list_copy = list.copy()
        list_copy.reverse()
        return list_copy


class RandomOrderingStrategy(TicketOrderingStrategy):
    def create_ordering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        list_copy = list.copy()
        random.shuffle(list_copy)
        return list_copy


class BlackHoleStrategy(TicketOrderingStrategy):
    def create_ordering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        return []
```

---

```python
class CustomerSupport:

    def __init__(self, processing_strategy: TicketOrderingStrategy):
        self.tickets = []
        self.processing_strategy = processing_strategy

    def create_ticket(self, customer, issue):
        self.tickets.append(SupportTicket(customer, issue))

    def process_tickets(self):
        # create the ordered list
        ticket_list = self.processing_strategy.create_ordering(self.tickets)

        # if it's empty, don't do anything
        if len(ticket_list) == 0:
            print("There are no tickets to process. Well done!")
            return

        # go through the tickets in the list
        for ticket in ticket_list:
            self.process_ticket(ticket)

    def process_ticket(self, ticket: SupportTicket):
        print("==================================")
        print(f"Processing ticket id: {ticket.id}")
        print(f"Customer: {ticket.customer}")
        print(f"Issue: {ticket.issue}")
        print("==================================")
```

---

```python
strategy = RandomOrderingStrategy()

app = CustomerSupport(strategy)

app.create_ticket("John Smith", "My computer makes strange sounds!")
app.create_ticket("Linus Sebastian", "I can't upload any videos, please help.")
app.create_ticket("Arjan Egges", "VSCode doesn't automatically solve my bugs.")

app.process_tickets()
```

---

# Задание лр / на неделю / ДЗ

1. Прочитать доступные источники по теме занятия

2. Найти в коде своих лабораторных работ проблемы, решаемые паттерном стратегия

3. Предложить улучшение кода лабораторных работ.

4. Задокументировать результаты и добавить в семестровый отчет.

---

# Источники

1. https://refactoring.guru/ru
2. https://www.youtube.com/watch?v=WQ8bNdxREHU
3. 
4. ...